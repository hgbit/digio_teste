# Digio-service
An AccountingEntry service.

![](doc/project_logo.png)

# Used Frameworks

This project is based on the following frameworks:

* **Lombok** - Avoid boiler plate code, increasing productivity. Works very well with builder pattern and Jackson Serialization/Deserialization.

* **MapStruct** - Fits well for mapping objects from web request layer and application domain.

* **Spring Boot Web** - Brings all dependencies required to web layer

* **String Data JPA** - Increases productivity with ORM and _"query methods"_, where in this case no JPQL query or JDBC query is necessary.

* **Flyway** - Controls database versions and run database migrations("when necessary").

* **Cucumber** - Behavior Driven Development approach, increases all team communication(same language for all) including PO's/PM's and other business professionals in the development process.
 
* **Mockito** - Allows to create and run tests without any external dependency. Eg: Postgresql available database is not required to run tests.
e
* **Junit** - Allows to verify assertions from tests scenarios.


# Documentation
This API is described in Open API 3.0(Swagger) and a doc to try it is located [here](doc/)

# Building and Running
To build go to the project root and:
`./gradlew build`

Docker is required to run:
`docker-compose up --build`

To debug in inteliJ and use a Docker postgresql container, run command below:
`docker run -e POSTGRES_PASSWORD=mysecretpassword --name postgres-local -d -p=5432:5432 postgres`
