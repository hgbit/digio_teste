package br.com.digio.service.data.repository;

import br.com.digio.service.data.entity.AccountingEntry;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.UUID;

@Repository
public interface AccountingEntryRepository extends JpaRepository<AccountingEntry, UUID> {

    List<AccountingEntry> findAllByAccountingAccount(Long accountingAccount);
}
