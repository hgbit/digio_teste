package br.com.digio.service.data.dto;


import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonPOJOBuilder;
import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.ToString;

import java.math.BigDecimal;

@Getter
@ToString
@EqualsAndHashCode
@AllArgsConstructor(access = AccessLevel.PRIVATE)
@Builder(builderClassName = "AccountingEntryResponseBuilder", toBuilder = true)
@JsonDeserialize(builder = AccountingEntryResponse.AccountingEntryResponseBuilder.class)
@JsonInclude(JsonInclude.Include.NON_NULL)
public class AccountingEntryResponse {

    private final Long contaContabil;

    private final Long data;

    private final BigDecimal valor;

    @JsonPOJOBuilder(withPrefix = "")
    @JsonIgnoreProperties(ignoreUnknown = true)
    public static class AccountingEntryResponseBuilder {

    }
}
