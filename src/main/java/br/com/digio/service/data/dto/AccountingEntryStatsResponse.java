package br.com.digio.service.data.dto;


import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonPOJOBuilder;
import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.ToString;

import java.math.BigDecimal;

@Getter
@ToString
@EqualsAndHashCode
@AllArgsConstructor(access = AccessLevel.PRIVATE)
@Builder(builderClassName = "AccountingEntryStatsResponseBuilder", toBuilder = true)
@JsonDeserialize(builder = AccountingEntryStatsResponse.AccountingEntryStatsResponseBuilder.class)
@JsonInclude(JsonInclude.Include.NON_NULL)
public class AccountingEntryStatsResponse {

    private final BigDecimal soma;

    private final BigDecimal min;

    private final BigDecimal max;

    private final BigDecimal media;

    private final Integer qtde;

    @JsonPOJOBuilder(withPrefix = "")
    @JsonIgnoreProperties(ignoreUnknown = true)
    public static class AccountingEntryStatsResponseBuilder {

    }
}
