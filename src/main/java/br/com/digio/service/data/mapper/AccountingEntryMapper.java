package br.com.digio.service.data.mapper;

import br.com.digio.service.data.dto.AccountingEntryCreationResponse;
import br.com.digio.service.data.dto.AccountingEntryRequest;
import br.com.digio.service.data.dto.AccountingEntryResponse;
import br.com.digio.service.data.entity.AccountingEntry;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;

@Mapper(componentModel = "spring")
public interface AccountingEntryMapper {

    @Mapping(target = "accountingAccount", source = "contaContabil")
    @Mapping(target = "date", source = "data")
    @Mapping(target = "value", source = "valor")
    @Mapping(target = "id", ignore = true)
    AccountingEntry targetToSource(AccountingEntryRequest accountingEntryRequest);

    @Mapping(target = "contaContabil", source = "accountingAccount")
    @Mapping(target = "data", source = "date")
    @Mapping(target = "valor", source = "value")
    AccountingEntryResponse sourceToTarget(AccountingEntry accountingEntry);

    AccountingEntryCreationResponse sourceToCreationResponse(AccountingEntry accountingEntry);
}
