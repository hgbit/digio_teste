package br.com.digio.service.data.dto;


import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonPOJOBuilder;
import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.ToString;

import javax.validation.constraints.NotNull;
import java.math.BigDecimal;
import java.util.Date;

@Getter
@ToString
@EqualsAndHashCode
@AllArgsConstructor(access = AccessLevel.PRIVATE)
@Builder(builderClassName = "AccountingEntryRequestBuilder", toBuilder = true)
@JsonDeserialize(builder = AccountingEntryRequest.AccountingEntryRequestBuilder.class)
public class AccountingEntryRequest {

    @NotNull
    private final Long contaContabil;

    @NotNull
    private final Long data;

    @NotNull
    private final BigDecimal valor;

    @JsonPOJOBuilder(withPrefix = "")
    @JsonIgnoreProperties(ignoreUnknown = true)
    public static class AccountingEntryRequestBuilder {

    }
}
