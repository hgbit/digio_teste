package br.com.digio.service.data.dto;


import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonPOJOBuilder;
import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.ToString;

import java.util.UUID;

@Getter
@ToString
@EqualsAndHashCode
@AllArgsConstructor(access = AccessLevel.PRIVATE)
@Builder(builderClassName = "AccountingEntryCreationResponseBuilder", toBuilder = true)
@JsonDeserialize(builder = AccountingEntryCreationResponse.AccountingEntryCreationResponseBuilder.class)
@JsonInclude(JsonInclude.Include.NON_NULL)
public class AccountingEntryCreationResponse {

    private final UUID id;

    @JsonPOJOBuilder(withPrefix = "")
    @JsonIgnoreProperties(ignoreUnknown = true)
    public static class AccountingEntryCreationResponseBuilder {

    }
}
