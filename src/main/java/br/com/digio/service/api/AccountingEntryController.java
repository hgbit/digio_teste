package br.com.digio.service.api;

import br.com.digio.service.data.dto.AccountingEntryCreationResponse;
import br.com.digio.service.data.dto.AccountingEntryRequest;
import br.com.digio.service.data.dto.AccountingEntryResponse;
import br.com.digio.service.data.dto.AccountingEntryStatsResponse;
import br.com.digio.service.service.AccountingEntryService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;
import java.util.List;
import java.util.UUID;

@RestController
@RequestMapping(value = "/lancamentos-contabeis", produces = MediaType.APPLICATION_JSON_VALUE)
public class AccountingEntryController {

    private static final Logger logger = LoggerFactory.getLogger(AccountingEntryController.class);

    private final AccountingEntryService accountingEntryService;

    public AccountingEntryController(AccountingEntryService accountingEntryService) {
        this.accountingEntryService = accountingEntryService;
    }

    @PostMapping(consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseStatus(HttpStatus.CREATED)
    public AccountingEntryCreationResponse createAccoutingEntry(@RequestBody @Valid AccountingEntryRequest accountingEntryRequest) {
        logger.info("Received request for creating accounting entry in account: {}", accountingEntryRequest.getContaContabil());
        return accountingEntryService.createAccoutingEntry(accountingEntryRequest);
    }

    @GetMapping("/{id}")
    public AccountingEntryResponse bindById(@PathVariable UUID id) {
        logger.info("Received request for finding accounting entry in by id: {}", id);
        return accountingEntryService.findById(id);
    }

    @GetMapping
    public List<AccountingEntryResponse> findAll(@RequestParam(required = false, value = "contaContabil") Long contaContabil) {
        logger.info("Received request for listing account entries by accountingAccount: {}", contaContabil);
        return accountingEntryService.findAllByAccountingAccount(contaContabil);
    }

    @GetMapping("/stats")
    public AccountingEntryStatsResponse findAllStats(@RequestParam(required = false, value = "contaContabil") Long contaContabil) {
        logger.info("Received request for finding account entries stats by accountingAccount: {}", contaContabil);
        return accountingEntryService.findStatsByAccountingAccount(contaContabil);
    }
}
