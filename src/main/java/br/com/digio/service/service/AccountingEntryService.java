package br.com.digio.service.service;

import br.com.digio.service.data.dto.AccountingEntryCreationResponse;
import br.com.digio.service.data.dto.AccountingEntryRequest;
import br.com.digio.service.data.dto.AccountingEntryResponse;
import br.com.digio.service.data.dto.AccountingEntryStatsResponse;
import br.com.digio.service.data.entity.AccountingEntry;
import br.com.digio.service.data.mapper.AccountingEntryMapper;
import br.com.digio.service.data.repository.AccountingEntryRepository;
import br.com.digio.service.exception.EntityNotFoundException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Optional;
import java.util.UUID;
import java.util.concurrent.atomic.AtomicReference;
import java.util.stream.Collectors;

@Service
public class AccountingEntryService {

    private final AccountingEntryRepository accountingEntryRepository;
    private final AccountingEntryMapper accountingEntryMapper;
    private final AccountingEntryStatsCalculationService accountingEntryStatsCalculationService;

    @Autowired
    public AccountingEntryService(AccountingEntryRepository accountingEntryRepository, AccountingEntryMapper accountingEntryMapper,
                                  AccountingEntryStatsCalculationService accountingEntryStatsCalculationService) {
        this.accountingEntryRepository = accountingEntryRepository;
        this.accountingEntryMapper = accountingEntryMapper;
        this.accountingEntryStatsCalculationService = accountingEntryStatsCalculationService;
    }

    @Transactional(propagation = Propagation.REQUIRED)
    public AccountingEntryCreationResponse createAccoutingEntry(AccountingEntryRequest accountingEntryRequest) {
        AccountingEntry accountingEntry = accountingEntryRepository.save(accountingEntryMapper.targetToSource(accountingEntryRequest));

        return accountingEntryMapper.sourceToCreationResponse(accountingEntry);
    }

    @Transactional(readOnly = true)
    public List<AccountingEntryResponse> findAllByAccountingAccount(Long accountingAccount) {
        List<AccountingEntry> accountingEntryList = findAccountEntryListDinamically(accountingAccount);

        return accountingEntryList.stream()
                                  .map(accountingEntryMapper::sourceToTarget)
                                  .collect(Collectors.toList());
    }

    @Transactional(readOnly = true)
    public AccountingEntryResponse findById(UUID id) {
        AccountingEntry accountingEntry = accountingEntryRepository.findById(id)
                                                                   .orElseThrow(() -> new EntityNotFoundException(String.format("Lancamento contabil: [%s] nao encontrado.", id)));

        return accountingEntryMapper.sourceToTarget(accountingEntry);
    }

    @Transactional(readOnly = true)
    public AccountingEntryStatsResponse findStatsByAccountingAccount(Long accountingAccount) {
        List<AccountingEntry> accountingEntryList = findAccountEntryListDinamically(accountingAccount);

        return accountingEntryStatsCalculationService.findStatsFromEntryList(accountingEntryList);
    }

    private List<AccountingEntry> findAccountEntryListDinamically(Long accountingAccount) {
        AtomicReference<List<AccountingEntry>> accountingEntryList = new AtomicReference<>();
        Optional.ofNullable(accountingAccount)
                .ifPresentOrElse(acc -> accountingEntryList.set(accountingEntryRepository.findAllByAccountingAccount(acc)), () -> accountingEntryList.set(accountingEntryRepository.findAll()));
        return accountingEntryList.get();
    }

}
