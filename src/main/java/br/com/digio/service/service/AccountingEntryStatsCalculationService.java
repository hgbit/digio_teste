package br.com.digio.service.service;

import br.com.digio.service.data.dto.AccountingEntryStatsResponse;
import br.com.digio.service.data.entity.AccountingEntry;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.Comparator;
import java.util.List;
import java.util.Objects;

@Service
public class AccountingEntryStatsCalculationService {

    public AccountingEntryStatsResponse findStatsFromEntryList(List<AccountingEntry> accountingEntries) {
        int size = accountingEntries.size();
        BigDecimal sum = findSumFromAccountingEntries(accountingEntries);
        BigDecimal average = accountingEntries.isEmpty() ? BigDecimal.ZERO : sum.divide(new BigDecimal(size), RoundingMode.HALF_UP);

        return AccountingEntryStatsResponse.builder()
                                           .max(this.findMaxFromAccountingEntries(accountingEntries))
                                           .min(this.findMinFromAccountingEntries(accountingEntries))
                                           .soma(sum)
                                           .media(average)
                                           .qtde(size)
                                           .build();
    }

    private BigDecimal findSumFromAccountingEntries(List<AccountingEntry> accountingEntries) {
        return accountingEntries.stream()
                                .map(Objects::requireNonNull)
                                .map(AccountingEntry::getValue)
                                .reduce(BigDecimal.ZERO, BigDecimal::add);
    }

    private BigDecimal findMinFromAccountingEntries(List<AccountingEntry> accountingEntries) {
        return accountingEntries.stream()
                                .map(Objects::requireNonNull)
                                .map(AccountingEntry::getValue)
                                .min(Comparator.naturalOrder())
                                .orElse(BigDecimal.ZERO);
    }

    private BigDecimal findMaxFromAccountingEntries(List<AccountingEntry> accountingEntries) {
        return accountingEntries.stream()
                                .map(Objects::requireNonNull)
                                .map(AccountingEntry::getValue)
                                .max(Comparator.naturalOrder())
                                .orElse(BigDecimal.ZERO);
    }

}
