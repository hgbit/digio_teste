package br.com.digio.service;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class DigioServiceApplication {

	public static void main(String[] args) {
		SpringApplication.run(DigioServiceApplication.class, args);
	}

}
