CREATE TABLE accounting_entry
(
    id                  uuid PRIMARY KEY,
    accounting_account  bigint NOT NULL,
    date                bigint NOT NULL,
    value               decimal(10, 2) NOT NULL
);
                                                                                            