package br.com.digio.service;

import io.cucumber.junit.Cucumber;
import io.cucumber.junit.CucumberOptions;
import org.junit.runner.RunWith;

@RunWith(Cucumber.class)
@CucumberOptions(
        features = "src/test/resources/features",
        glue = "br.com.digio.service",
        plugin = "pretty",
        strict = true)
public class DigioServiceApplicationTests {
}
