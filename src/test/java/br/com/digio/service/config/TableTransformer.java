package br.com.digio.service.config;

import br.com.digio.service.data.dto.AccountingEntryRequest;
import br.com.digio.service.data.entity.AccountingEntry;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import io.cucumber.java.DataTableType;

import java.util.Map;

public class TableTransformer {

    private final ObjectMapper objectMapper;

    public TableTransformer(ObjectMapper objectMapper) {
        this.objectMapper = objectMapper;
    }

    @DataTableType
    public AccountingEntry defineAccoutingEntry(Map<String, String> entry) throws JsonProcessingException {
        return objectMapper.readValue(objectMapper.writeValueAsString(entry), AccountingEntry.class);
    }

    @DataTableType
    public AccountingEntryRequest defineAccoutingEntryRequest(Map<String, String> entry) throws JsonProcessingException {
        return objectMapper.readValue(objectMapper.writeValueAsString(entry), AccountingEntryRequest.class);
    }

}
