package br.com.digio.service;

import br.com.digio.service.data.repository.AccountingEntryRepository;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;
import com.fasterxml.jackson.datatype.jsr310.JSR310Module;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.boot.test.mock.mockito.MockBeans;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.converter.json.Jackson2ObjectMapperBuilder;

import static com.fasterxml.jackson.core.JsonGenerator.Feature.WRITE_BIGDECIMAL_AS_PLAIN;

@Configuration
@MockBeans({@MockBean(AccountingEntryRepository.class)})
public class DigioServiceTestConfig {

    @Bean
    public DigioServiceTestData apiServiceTestData() {
        return new DigioServiceTestData();
    }

    @Bean
    public ObjectMapper objectMapper(Jackson2ObjectMapperBuilder builder) {
        return builder.featuresToEnable(WRITE_BIGDECIMAL_AS_PLAIN)
                      .modules(new JSR310Module())
                      .featuresToDisable(SerializationFeature.WRITE_DATES_AS_TIMESTAMPS)
                      .build();
    }
}
