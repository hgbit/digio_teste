package br.com.digio.service.steps;

import br.com.digio.service.DigioServiceApplication;
import br.com.digio.service.DigioServiceTestData;
import br.com.digio.service.data.dto.AccountingEntryCreationResponse;
import br.com.digio.service.data.dto.AccountingEntryResponse;
import br.com.digio.service.data.entity.AccountingEntry;
import br.com.digio.service.data.repository.AccountingEntryRepository;
import br.com.digio.service.exception.SystemException;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import io.cucumber.java.After;
import io.cucumber.java.Before;
import io.cucumber.java.en.And;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import io.cucumber.spring.CucumberContextConfiguration;
import org.junit.Assert;
import org.mockito.ArgumentCaptor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.ResultActions;
import org.springframework.test.web.servlet.request.MockHttpServletRequestBuilder;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;

import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.UUID;

import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.BDDMockito.given;
import static org.mockito.BDDMockito.then;
import static org.mockito.Mockito.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@SpringBootTest
@ContextConfiguration(classes = {DigioServiceApplication.class})
@AutoConfigureMockMvc
@ActiveProfiles("test")
@CucumberContextConfiguration
public class AccountingEntriesSteps {

    private final AccountingEntryRepository accountingEntryRepository;
    private final MockMvc mockMvc;
    private final ObjectMapper objectMapper;
    private final DigioServiceTestData testData;

    @Autowired
    public AccountingEntriesSteps(AccountingEntryRepository accountingEntryRepository, MockMvc mockMvc, ObjectMapper objectMapper, DigioServiceTestData testData) {
        this.accountingEntryRepository = accountingEntryRepository;
        this.mockMvc = mockMvc;
        this.objectMapper = objectMapper;
        this.testData = testData;
    }

    @Before
    public void setUp() {
        this.testData.reset();
    }

    @After
    public void wrapUp() {
        reset(accountingEntryRepository);
    }

    @Given("this are the accounting entries currently in the database:")
    public void thisAreTheAccountingEntriesCurrentlyInTheDatabase(List<AccountingEntry> accountingEntries) {
        given(this.accountingEntryRepository.findAll()).willReturn(accountingEntries);

        accountingEntries.forEach(accountingEntry -> given(this.accountingEntryRepository.findById(eq(accountingEntry.getId()))).willReturn(Optional.of(accountingEntry)));
    }

    @When("this user requests accouting entries")
    public void thisUserRequestsAccoutingEntries() throws Exception {
        MockHttpServletRequestBuilder
                mockHttpServletRequestBuilder =
                MockMvcRequestBuilders.get("/lancamentos-contabeis")
                                      .accept(MediaType.APPLICATION_JSON);


        ResultActions resultActions = mockMvc.perform(mockHttpServletRequestBuilder);

        this.testData.setResultActions(resultActions);
    }

    @When("this user request accouting entry: {string}")
    public void thisUserRequestAccoutingEntry(String accoutingEntryId) throws Exception {
        MockHttpServletRequestBuilder
                mockHttpServletRequestBuilder =
                MockMvcRequestBuilders.get("/lancamentos-contabeis/" + accoutingEntryId)
                                      .accept(MediaType.APPLICATION_JSON);

        ResultActions resultActions = mockMvc.perform(mockHttpServletRequestBuilder);

        this.testData.setResultActions(resultActions);
    }

    @Then("the system will return {int} status code")
    public void theSystemWillReturnStatusCode(int status) throws Exception {
        this.testData.getResultActions()
                     .andExpect(status().is(status));
    }

    @Then("the service will reply this list of accouting entries: {string}")
    public void theServiceWillReplyThisListOfAccoutingEntries(String json) throws Exception {
        final TypeReference<List<AccountingEntryResponse>> listTypeReference = new TypeReference<>() {
        };

        List<AccountingEntryResponse> expectedResponse = objectMapper.readValue(json, listTypeReference);

        MvcResult mvcResult = this.testData.getResultActions()
                                           .andReturn();

        List<AccountingEntryResponse> accountingEntryResponses = objectMapper.readValue(mvcResult.getResponse()
                                                                                                 .getContentAsString(), listTypeReference);

        Assert.assertEquals(expectedResponse, accountingEntryResponses);
    }

    @Then("the service will reply this accouting entry: {string}")
    public void theServiceWillReplyThisAccoutingEntry(String json) throws Exception {
        AccountingEntryResponse expectedResponse = objectMapper.readValue(json, AccountingEntryResponse.class);
        MvcResult mvcResult = this.testData.getResultActions()
                                           .andReturn();

        AccountingEntryResponse accountingEntryResponse = objectMapper.readValue(mvcResult.getResponse()
                                                                                          .getContentAsString(), AccountingEntryResponse.class);

        Assert.assertEquals(expectedResponse, accountingEntryResponse);
    }

    @Then("the service will reply this accouting entry creation response: {string}")
    public void theServiceWillReplyThisAccoutingEntryCreationResponse(String json) throws Exception {
        AccountingEntryCreationResponse expectedResponse = objectMapper.readValue(json, AccountingEntryCreationResponse.class);
        MvcResult mvcResult = this.testData.getResultActions()
                                           .andReturn();

        AccountingEntryCreationResponse accountingEntryResponse = objectMapper.readValue(mvcResult.getResponse()
                                                                                                  .getContentAsString(), AccountingEntryCreationResponse.class);

        Assert.assertEquals(expectedResponse, accountingEntryResponse);
    }

    @Then("the database is offline")
    public void theDatabaseIsOffline() {
        given(this.accountingEntryRepository.findAll()).willThrow(SystemException.class);
        given(this.accountingEntryRepository.findById(any(UUID.class))).willThrow(SystemException.class);
        given(this.accountingEntryRepository.save(any())).willThrow(SystemException.class);

    }

    @Then("the database will be called to list accouting entries")
    public void theDatabaseWillBeCalledToListAccountingEntries() {
        then(this.accountingEntryRepository).should(atLeastOnce())
                                            .findAll();
    }

    @Then("the database is called to find accouting entry id: {string}")
    public void theDatabaseIsCalledToFindAccoutingEntryId(String accountingEntryId) {
        then(this.accountingEntryRepository).should(only())
                                            .findById(eq(UUID.fromString(accountingEntryId)));
    }

    @Given("the user wants to create a new accouting entry as follows:")
    public void theUserWantsToCreateANewAccoutingEntryAsFollows(Map<String, String> accountingEntryRequest) throws JsonProcessingException {
        this.testData.setRequestJson(objectMapper.writeValueAsString(accountingEntryRequest));
    }


    @Given("the next accouting entry insertion id is: {string}")
    public void theNextAccoutingEntryInsertionIdIs(String id) {
        when(this.accountingEntryRepository.save(any())).thenAnswer(invocation -> {
            final AccountingEntry accountingEntry = invocation.getArgument(0);
            accountingEntry.setId(UUID.fromString(id));

            return accountingEntry;
        });
    }

    @When("this user request to create this accouting entry")
    public void thisUserRequestToCreateThisAccoutingEntry() throws Exception {
        MockHttpServletRequestBuilder
                mockHttpServletRequestBuilder =
                MockMvcRequestBuilders.post("/lancamentos-contabeis/")
                                      .accept(MediaType.APPLICATION_JSON)
                                      .contentType(MediaType.APPLICATION_JSON_VALUE)
                                      .content(this.testData.getRequestJson());

        ResultActions resultActions = mockMvc.perform(mockHttpServletRequestBuilder);
        this.testData.setResultActions(resultActions);
    }

    @And("the database is called to insert this accouting entry:")
    public void theDatabaseIsCalledToInsertThisAccoutingEntry(AccountingEntry expectedAccountingEntry) {
        final ArgumentCaptor<AccountingEntry> columnArgumentCaptor = ArgumentCaptor.forClass(AccountingEntry.class);

        then(accountingEntryRepository).should(only())
                                       .save(columnArgumentCaptor.capture());
        final AccountingEntry savedAccountingEntry = columnArgumentCaptor.getValue();

        Assert.assertEquals(expectedAccountingEntry, savedAccountingEntry);
    }

    @And("the database will be not called to insert nothing")
    public void theDatabaseWillBeNotCalledToInsertNothing() {
        then(this.accountingEntryRepository).should(never())
                                            .save(any());
    }
}
