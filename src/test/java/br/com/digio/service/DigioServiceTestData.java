package br.com.digio.service;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;
import org.springframework.test.web.servlet.ResultActions;

@Getter
@Setter
@ToString
public class DigioServiceTestData {

    private ResultActions resultActions;
    private String requestJson;

    public DigioServiceTestData() {
        this.reset();
    }

    public void reset() {
        this.resultActions = null;
        this.requestJson = null;
    }


}
