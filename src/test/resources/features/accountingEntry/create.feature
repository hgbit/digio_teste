@AccoutingEntries
Feature: Create new accounting entry
  Users want to create one accouting entry

  Scenario: Create an AccountingEntry without fails
    Given the user wants to create a new accouting entry as follows:
      | contaContabil | 1111001  |
      | data          | 20170130 |
      | valor         | 300.15   |
    And the next accouting entry insertion id is: "aca1f386-5d11-4fb5-9dc2-2009e6081111"
    When this user request to create this accouting entry
    And the database is called to insert this accouting entry:
      | id                                   | accountingAccount | date     | value  |
      | aca1f386-5d11-4fb5-9dc2-2009e6081111 | 1111001          | 20170130 | 300.15 |
    Then the system will return 201 status code
    And the service will reply this accouting entry: "{\"id\":\"aca1f386-5d11-4fb5-9dc2-2009e6081111\"}"

  Scenario: Create an AccountingEntry without value
    Given the user wants to create a new accouting entry as follows:
      | contaContabil | 1111001  |
      | data          | 20170130 |
    And the next accouting entry insertion id is: "aca1f386-5d11-4fb5-9dc2-2009e6081111"
    When this user request to create this accouting entry
    And the database will be not called to insert nothing
    Then the system will return 400 status code

  Scenario: Try to create a accouting entry with database offline
    Given the user wants to create a new accouting entry as follows:
      | contaContabil | 1111001  |
      | data          | 20170130 |
      | valor         | 300.15   |
    And the database is offline
    When this user request to create this accouting entry
    Then the system will return 500 status code