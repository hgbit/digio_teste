@AccoutingEntries
Feature: List accouting entries
  Users want to list accouting entries and may filter by accoutingAccount

  Scenario: Retrieve a list of accoutingEntries without fails
    Given this are the accounting entries currently in the database:
      | id                                   | accountingAccount | date     | value  |
      | aca1f386-5d11-4fb5-9dc2-2009e6081111 | 1111001           | 20170130 | 300.15 |
      | aca2f386-5d11-4fb5-9dc2-2009e6082222 | 1111001           | 20200819 | 150.55 |
      | aca3f386-5d11-4fb5-9dc2-2009e6083333 | 1111002           | 20200818 | 18.85  |
      | aca3f386-5d11-4fb5-9dc2-2009e6084444 | 1111003           | 20200810 | 200.0  |
    When this user requests accouting entries
    Then the system will return 200 status code
    And the database will be called to list accouting entries
    And the service will reply this list of accouting entries: "[{\"contaContabil\":1111001,\"data\":20170130,\"valor\":300.15},{\"contaContabil\":1111001,\"data\":20200819,\"valor\":150.55},{\"contaContabil\":1111002,\"data\":20200818,\"valor\":18.85},{\"contaContabil\":1111003,\"data\":20200810,\"valor\":200.0}]"

  Scenario: Try to retrieve a list of accounting entries with database offline
    Given the database is offline
    When this user requests accouting entries
    Then the system will return 500 status code