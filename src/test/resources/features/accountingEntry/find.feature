@AccoutingEntries
Feature: Find accounting entries by id
  Users want to get details about one accouting entry

  Scenario: Retrieve a AccountingEntry without fails
    Given this are the accounting entries currently in the database:
      | id                                   | accountingAccount | date     | value  |
      | aca1f386-5d11-4fb5-9dc2-2009e6081111 | 1111001           | 20170130 | 300.15 |
      | aca2f386-5d11-4fb5-9dc2-2009e6082222 | 1111001           | 20200819 | 150.55 |
      | aca3f386-5d11-4fb5-9dc2-2009e6083333 | 1111002           | 20200818 | 18.85  |
      | aca3f386-5d11-4fb5-9dc2-2009e6084444 | 1111003           | 20200810 | 200.0  |
    When this user request accouting entry: "aca1f386-5d11-4fb5-9dc2-2009e6081111"
    And the database is called to find accouting entry id: "aca1f386-5d11-4fb5-9dc2-2009e6081111"
    Then the system will return 200 status code
    And the service will reply this accouting entry: "{\"contaContabil\":1111001,\"data\":20170130,\"valor\":300.15}"

  Scenario: Retrieve a accouting entry that not exists
    Given this are the accounting entries currently in the database:
      | id                                   | accountingAccount | date     | value  |
      | aca1f386-5d11-4fb5-9dc2-2009e6081111 | 1111001           | 20170130 | 300.15 |
      | aca2f386-5d11-4fb5-9dc2-2009e6082222 | 1111001           | 20200819 | 150.55 |
      | aca3f386-5d11-4fb5-9dc2-2009e6083333 | 1111002           | 20200818 | 18.85  |
      | aca3f386-5d11-4fb5-9dc2-2009e6084444 | 1111003           | 20200810 | 200.0  |
    When this user request accouting entry: "aca3f386-5d11-4fb5-9dc2-2009e608000"
    Then the system will return 404 status code
    And the database is called to find accouting entry id: "aca3f386-5d11-4fb5-9dc2-2009e608000"

  Scenario: Try to retrieve a accouting entry with database offline
    Given the database is offline
    When this user request accouting entry: "aca1f386-5d11-4fb5-9dc2-2009e6085931"
    Then the system will return 500 status code